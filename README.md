###About
Wikipedia inverted index search with Hadoop and HBase.

### Running
run `mvn package` to get WikiSearch.jar

run `mvn compile assembly:single` to get WikiSearch-jar-with-dependencies.jar

Create Vocabulary and DF:
`hadoop jar WikiSearch.jar cz.cvut.bigdata.step1.BuildVocabulary --input /bigdata/enwiki-latest-pages-articles.txt --output vocabulary`

Filter Vocabulary (minDf and maxDf is relative to number of documents. #doc/df):
`java -cp WikiSearch.jar cz.cvut.bigdata.step2.FilterVocabulary --input vocabulary/part-r-00000 --output filteredVocabulary/all.txt --minDf 4 --maxDf 90000` 

Create inverse index:

`hadoop jar WikiSearch.jar cz.cvut.bigdata.step3.CreateInverseIndex --input /bigdata/enwiki-latest-pages-articles.txt --output inverseIndex --vocabularyCache filteredVocabulary/all.txt`


Combine doc2len files to one file:

`cat /hdfs/user/udrzalv/inverseIndex/doc2len-m-000* > /hdfs/user/udrzalv/inverseIndex/doc2len-all.txt`


Run search with inverse index from file:

`hadoop jar WikiSearch.jar cz.cvut.bigdata.step4.DocumentRetrieval --inverseIndex inverseIndex/byFirstLetter --vocabularyCache filteredVocabulary/all.txt --doc2len inverseIndex/doc2len-all.txt --output query --query "czech nagano"`


Run search with inverse index from HBase:

`java -cp WikiSearch-jar-with-dependencies.jar cz.cvut.bigdata.step4.DocumentRetrievalHbase --auth_conf jaas.conf --async_conf asynchbase.properties --query "icehockey" --doc2len /hdfs/user/udrzalv/inverseIndex/doc2len-all.txt --vocabularyCache /hdfs/user/udrzalv/filteredVocabulary/all.txt`


