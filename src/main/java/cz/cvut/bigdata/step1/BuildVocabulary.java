package cz.cvut.bigdata.step1;

import cz.cvut.bigdata.cli.ArgumentParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * WordCount Example, version 1.0
 * <p>
 * This is a very simple extension of basic WordCount Example implemented using
 * a new MapReduce API.
 */
public class BuildVocabulary extends Configured implements Tool {
    public static final String DOCUMENT_COUNTER_KEY = "#";

    /**
     * The main entry of the application.
     */
    public static void main(String[] arguments) throws Exception {
        System.exit(ToolRunner.run(new BuildVocabulary(), arguments));
    }

    /**
     * Receives (byteOffsetOfLine, textOfLine), note we do not care about the type of the key
     * because we do not use it anyway, and emits (word, 1) for each occurrence of the word
     * in the line of text (i.e. the received value).
     */
    public static class MyMapper extends Mapper<Object, Text, Text, IntWritable> {
        private final IntWritable ONE = new IntWritable(1);
        private Text word = new Text();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String[] words = value.toString().toLowerCase().split(" ");
            Iterator<String> wordIterator = Arrays.asList(words).iterator();
            Pattern pattern = Pattern.compile("[a-z]{3,25}");
            HashSet<String> hashSet = new HashSet<>();
            while (wordIterator.hasNext()) {
                String term = wordIterator.next();
                Matcher m = pattern.matcher(term);
                if (m.matches() && !hashSet.contains(term)) {
                    hashSet.add(term);
                    word.set(term);
                    context.write(word, ONE);
                }
            }
            context.write(new Text(DOCUMENT_COUNTER_KEY), ONE);
        }
    }

    /**
     * Receives (word, list[1, 1, 1, 1, ..., 1]) where the number of 1 corresponds to the total number
     * of times the word occurred in the input text, which is precisely the value the reducer emits along
     * with the original word as the key.
     * <p>
     * NOTE: The received list may not contain only 1s if a combiner is used.
     */
    public static class MyReducer extends Reducer<Text, IntWritable, Text, Text> {
        int documentCount = 0;
        public void reduce(Text text, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;

            for (IntWritable value : values) {
                sum += value.get();
            }
            if (text.toString().equals(DOCUMENT_COUNTER_KEY)) {
                documentCount = sum;
                return;
            }

            //context.write(text, new DoubleWritable((sum*1.0/documentCount)));
            context.write(text, new Text(sum + "\t" + ((documentCount * 1.0) / sum)));
        }
    }

    /**
     * This is where the MapReduce job is configured and being launched.
     */
    @Override
    public int run(String[] arguments) throws Exception {
        ArgumentParser parser = new ArgumentParser("WordCount");

        parser.addArgument("input", true, true, "specify input directory");
        parser.addArgument("output", true, true, "specify output directory");

        parser.parseAndCheck(arguments);

        Path inputPath = new Path(parser.getString("input"));
        Path outputDir = new Path(parser.getString("output"));

        // Create configuration.
        Configuration conf = getConf();

        // Using the following line instead of the previous 
        // would result in using the default configuration
        // settings. You would not have a change, for example,
        // to set the number of reduce tasks (to 5 in this
        // example) by specifying: -D mapred.reduce.tasks=5
        // when running the job from the console.
        //
        // Configuration conf = new Configuration(true);

        // Create job.
        Job job = Job.getInstance(conf, "BuildVocabulary");
        job.setJarByClass(MyMapper.class);

        // Setup MapReduce.
        job.setMapperClass(MyMapper.class);

        job.setReducerClass(MyReducer.class);
        // Make use of a combiner - in this simple case
        // it is the same as the reducer.
        //job.setCombinerClass(MyReducer.class);


        // By default, the number of reducers is configured
        // to be 1, similarly you can set up the number of
        // reducers with the following line.
        //
        // job.setNumReduceTasks(1);

        // Specify (key, value).
        job.setOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setOutputValueClass(Text.class);

        // Input.
        FileInputFormat.addInputPath(job, inputPath);
        job.setInputFormatClass(TextInputFormat.class);

        // Output.
        FileOutputFormat.setOutputPath(job, outputDir);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileSystem hdfs = FileSystem.get(conf);

        // Delete output directory (if exists).
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);

        // Execute the job.
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
