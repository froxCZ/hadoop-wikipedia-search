package cz.cvut.bigdata;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by frox on 14.5.16.
 */
public class VectorWritable implements Writable {

    private List<Integer> keys = new ArrayList<>();

    /**
     * Values (TF/TF-IDF) of non-zero elements, i.e. articles containing the term.
     */
    private List<Double> values = new ArrayList<>();

    @Override
    public void write(DataOutput output) throws IOException {
        if (keys.size() != values.size()) {
            throw new RuntimeException("keys.size != values.size");
        }
        output.writeInt(keys.size());
        for (int i = 0; i < keys.size(); i++) {
            output.writeInt(keys.get(i));
            output.writeDouble(values.get(i));
        }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        int size = in.readInt();
        keys = new ArrayList<>(size);
        values = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            keys.add(i, in.readInt());
            values.add(i, in.readDouble());
        }
    }

    public void addPair(Integer key, Double value) {
        keys.add(key);
        values.add(value);
    }

    public List<Integer> getKeys() {
        return keys;
    }

    public List<Double> getValues() {
        return values;
    }

    public int size() {
        return keys.size();
    }
}
