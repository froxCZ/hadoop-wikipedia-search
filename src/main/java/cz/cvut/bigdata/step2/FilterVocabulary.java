package cz.cvut.bigdata.step2;

import cz.cvut.bigdata.cli.ArgumentParser;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by frox on 13.5.16.
 */
public class FilterVocabulary {

    /**
     * The main entry of the application.
     */
    public static void main(String[] arguments) throws Exception {
        ArgumentParser parser = new ArgumentParser("WordCount");

        parser.addArgument("input", true, true, "specify input directory");
        parser.addArgument("output", true, true, "specify output directory");
        parser.addArgument("minDf", true, true, "specify min Df output directory");
        parser.addArgument("maxDf", true, true, "specify max Df output directory");

        parser.parseAndCheck(arguments);

        int minDf = parser.getInt("minDf");
        int maxDf = parser.getInt("maxDf");

        Path inputPath = new Path(parser.getString("input"));

        FileInputStream fstream = new FileInputStream(parser.getString("input"));
        File outFile = new File(parser.getString("output"));
        outFile.getParentFile().mkdirs();
        outFile.createNewFile();

        OutputStreamWriter outWriter = new OutputStreamWriter(new FileOutputStream(outFile));

        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;

        while ((strLine = br.readLine()) != null) {
            String[] recSplit = strLine.split("\t");
            double relativeOccurence = Double.valueOf(recSplit[2]);
            if (relativeOccurence > minDf && relativeOccurence < maxDf) {
                outWriter.write(recSplit[0] + "\t" + recSplit[1] + "\n");
            }

        }
        outWriter.flush();
        outWriter.close();
        br.close();
    }
}
