package cz.cvut.bigdata;

/**
 * Created by frox on 14.5.16.
 */
public class Util {
    public static String getValueBeforeFirstTab(String textStr) {
        int firstTab = textStr.indexOf("\t");
        return textStr.substring(0, firstTab);
    }

    public static String getValueAfterFirstTab(String textStr) {
        int firstTab = textStr.indexOf("\t");
        return textStr.substring(firstTab+1, textStr.length());
    }
}
