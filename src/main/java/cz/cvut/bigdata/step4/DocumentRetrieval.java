package cz.cvut.bigdata.step4;

import cz.cvut.bigdata.Util;
import cz.cvut.bigdata.cli.ArgumentParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * WordCount Example, version 1.0
 * <p>
 * This is a very simple extension of basic WordCount Example implemented using
 * a new MapReduce API.
 */
public class DocumentRetrieval extends Configured implements Tool {
    public static final String KEY_VALUE_SEPARATOR = ":";
    private static final String QUERY = "query";
    private Path outputDir;


    /**
     * The main entry of the application.
     */
    public static void main(String[] arguments) throws Exception {
        DocumentRetrieval documentRetrieval = new DocumentRetrieval();
        int ret = ToolRunner.run(documentRetrieval, arguments);
        Thread.sleep(1000);
        if (ret == 0) {
            documentRetrieval.printBestResults();
        }

    }

    private void printBestResults() throws IOException {
        FileSystem hdfs = FileSystem.get(getConf());
        hdfs.open(new Path(outputDir.toString() + "/part-r-00000"));
        BufferedReader cacheReader = new BufferedReader(new InputStreamReader(hdfs.open(new Path(outputDir.toString() + "/part-r-00000"))));//vocabulary is first
        String strLine;
        List<FoundDocument> foundDocuments = new ArrayList<>();
        while ((strLine = cacheReader.readLine()) != null) {
            String[] doc_score = strLine.split("\t");
            foundDocuments.add(new FoundDocument(Integer.valueOf(doc_score[0]), Double.valueOf(doc_score[1])));
        }
        Collections.sort(foundDocuments);
        int i = 30;
        for (FoundDocument document : foundDocuments) {
            if (i == 0) {
                break;
            }
            if (i > 20) {
                System.out.print(document.getWikiPageTitle() + " ");
            }
            System.out.println("score:" + document.score + " https://en.wikipedia.org/?curid=" + document.id);
            i--;
        }
    }


    /**
     * Receives (byteOffsetOfLine, textOfLine), note we do not care about the type of the key
     * because we do not use it anyway, and emits (word, 1) for each occurrence of the word
     * in the line of text (i.e. the received value).
     */
    public static class MyMapper extends Mapper<Object, Text, IntWritable, Text> {
        private String[] queryTerms;

        protected void setup(Context context) throws IOException, InterruptedException {
            super.setup(context);
            queryTerms = context.getConfiguration().get(QUERY).split(" ");
        }

        public void map(Object key, Text text, Context context) throws IOException, InterruptedException {
            String textStr = text.toString();
            String term = Util.getValueBeforeFirstTab(textStr);

            for (String q : queryTerms) {
                if (q.equals(term)) {
                    String list = Util.getValueAfterFirstTab(textStr);
                    String[] listPairs = list.split(",");
                    for (String pair : listPairs) {
                        String[] doc_frequency = pair.split(KEY_VALUE_SEPARATOR);
                        context.write(new IntWritable(Integer.valueOf(doc_frequency[0])), new Text(term + KEY_VALUE_SEPARATOR + doc_frequency[1]));
                    }
                    break;
                }
            }


        }
    }

    /**
     */
    public static class MyReducer extends Reducer<IntWritable, Text, IntWritable, DoubleWritable> {
        Double k = 1.5;
        Double b = 0.75;
        Double avgdl;
        Long N;
        HashMap<Integer, Integer> doc2len = new HashMap<>();
        HashMap<String, Integer> termToDocCount = new HashMap<>();

        protected void setup(Reducer.Context context) throws IOException, InterruptedException {
            super.setup(context);
            Configuration conf = context.getConfiguration();
            FileSystem fs = FileSystem.getLocal(conf);

            Path[] dataFile = DistributedCache.getLocalCacheFiles(conf);
            BufferedReader cacheReader = new BufferedReader(new InputStreamReader(fs.open(dataFile[0])));//vocabulary is first
            String strLine;
            while ((strLine = cacheReader.readLine()) != null) {
                String[] split = strLine.split("\t");
                termToDocCount.put(split[0], Integer.valueOf(split[1]));
            }

            cacheReader = new BufferedReader(new InputStreamReader(fs.open(dataFile[1])));//doc2len file is second
            BigDecimal docLenSum = BigDecimal.ZERO;
            N = 0L;
            while ((strLine = cacheReader.readLine()) != null) {
                String[] split = strLine.split("\t");
                doc2len.put(Integer.valueOf(split[0]), Integer.valueOf(split[1]));
                docLenSum = docLenSum.add(BigDecimal.valueOf(Integer.valueOf(split[1])));
                N++;
            }
            System.out.println("#doc :" + N);
            avgdl = docLenSum.divide(BigDecimal.valueOf(N), RoundingMode.HALF_UP).doubleValue();
            System.out.println(docLenSum + " avgdl:" + avgdl);
            cacheReader.close();
        }

        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            Double score = 0.0;
            for (Text t : values) {
                String[] term_freq = t.toString().split(KEY_VALUE_SEPARATOR);
                String term = term_freq[0];
                int freq = Integer.valueOf(term_freq[1]);
                Double up = freq * (k + 1);
                Double down = freq + k * (1 - b + b * doc2len.get(Integer.valueOf(key.toString())) / avgdl);
                Double IDF = Math.log((N - termToDocCount.get(term) + 0.5) / (termToDocCount.get(term) + .5));
                score += IDF * up / down;
            }
            context.write(key, new DoubleWritable(score));
        }

        private double calculateBM25Score(int documentId, String term, int freq) {
            Double up = freq * (k + 1);
            Double down = freq + k * (1 - b + b * doc2len.get(documentId) / avgdl);
            Double IDF = Math.log((N - termToDocCount.get(term) + 0.5) / (termToDocCount.get(term) + .5));
            return IDF * up / down;
        }

    }


    /**
     * This is where the MapReduce job is configured and being launched.
     */
    @Override
    public int run(String[] arguments) throws Exception {
        ArgumentParser parser = new ArgumentParser("DocumentRetrieval");

        parser.addArgument("inverseIndex", true, true, "specify input directory");
        parser.addArgument("output", true, true, "specify output directory");
        parser.addArgument("vocabularyCache", true, true, "specify ");
        parser.addArgument("doc2len", true, true, "specify ");
        parser.addArgument(QUERY, true, true, "specify ");

        parser.parseAndCheck(arguments);
        String query = parser.getString(QUERY);

        Path inverseIndex = new Path(parser.getString("inverseIndex"));
        outputDir = new Path(parser.getString("output"));
        Path cachePath = new Path(parser.getString("vocabularyCache"));
        Path doc2lenPath = new Path(parser.getString("doc2len"));

        // Create configuration.
        Configuration conf = getConf();

        // Using the following line instead of the previous 
        // would result in using the default configuration
        // settings. You would not have a change, for example,
        // to set the number of reduce tasks (to 5 in this
        // example) by specifying: -D mapred.reduce.tasks=5
        // when running the job from the console.
        //
        // Configuration conf = new Configuration(true);

        // Create job.
        DistributedCache.addCacheFile(cachePath.toUri(), conf);
        DistributedCache.addCacheFile(doc2lenPath.toUri(), conf);
        conf.set(QUERY, parser.getString(QUERY));
        Job job = Job.getInstance(conf, "DocumentRetrieval");

        job.setJarByClass(MyMapper.class);

        // Setup MapReduce.
        job.setMapperClass(MyMapper.class);

        job.setReducerClass(MyReducer.class);
        // Make use of a combiner - in this simple case
        // it is the same as the reducer.
        //job.setCombinerClass(MyReducer.class);


        // By default, the number of reducers is configured
        // to be 1, similarly you can set up the number of
        // reducers with the following line.
        //
        // job.setNumReduceTasks(1);

        // Specify (key, value).
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(DoubleWritable.class);

        // Input.
        //load inverse indexes for each query starting letter.
        String[] queryTerms = query.split(" ");
        HashSet<String> firstLetters = new HashSet<>();
        for (String q : queryTerms) {
            firstLetters.add("" + q.charAt(0));
        }
        Iterator<String> it = firstLetters.iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            String input = inverseIndex + "/" + it.next() + "-r-00000";
            sb.append(input + ",");
        }
        String paths = sb.toString();
        paths = paths.substring(0, paths.length() - 1);
        System.out.println("inputPaths: " + paths);
        FileInputFormat.addInputPaths(job, paths);

        job.setInputFormatClass(TextInputFormat.class);

        // Output.
        FileOutputFormat.setOutputPath(job, outputDir);
        job.setOutputFormatClass(TextOutputFormat.class);


        FileSystem hdfs = FileSystem.get(conf);

        // Delete output directory (if exists).
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);

        // Execute the job.
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
