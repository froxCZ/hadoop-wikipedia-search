package cz.cvut.bigdata.step4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by frox on 16.5.16.
 */
class FoundDocument implements Comparable {
    int id;
    Double score;

    public FoundDocument(int id, Double score) {
        this.id = id;
        this.score = score;
    }

    @Override
    public int compareTo(Object o) {
        FoundDocument other = (FoundDocument) o;
        return other.score.compareTo(score);
    }

    public String getWikiPageTitle() {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;

        try {
            url = new URL("https://en.wikipedia.org/?curid=" + id);
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                int titleIndex = line.indexOf("<title>");
                if (titleIndex >= 0) {
                    is.close();
                    return line.substring(titleIndex+"<title>".length(),line.length()-" - Wikipedia, the free encyclopedia</title>".length());
                }
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        return "x";
    }

    @Override
    public String toString() {
        return "FoundDocument{" +
                "id=" + id +
                ", score=" + score +
                '}';
    }
}
