package cz.cvut.bigdata.step4;

import cz.cvut.bigdata.cli.ArgumentParser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.cvut.bigdata.step3.CreateInverseIndexHbase;
import org.hbase.async.Config;
import org.hbase.async.GetRequest;
import org.hbase.async.HBaseClient;
import org.hbase.async.KeyValue;

/**
 * Created by frox on 16.5.16.
 */
public class DocumentRetrievalHbase {
    private long N;
    private double avgdl;
    Double k = 1.5;
    Double b = 0.75;
    HashMap<Integer, Double> docScores = new HashMap<>();
    HashMap<Integer, Integer> doc2len = new HashMap<>();
    HashMap<String, Integer> termToDocCount = new HashMap<>();
    private HBaseClient client;
    private String table;

    public static void main(String[] args) throws Exception {
        DocumentRetrievalHbase documentRetrieval = new DocumentRetrievalHbase();
        documentRetrieval.run(args);
    }

    private void run(String[] args) throws IOException {
        //-- PARSE ARGUMENTS --//
        ArgumentParser parser = new ArgumentParser("DocumentRetrievalHbase");
        parser.addArgument("async_conf", true, true, "specify path to asynchbase.properties");
        parser.addArgument("auth_conf", true, true, "specify path to java.security.auth.login.config");
        parser.addArgument("query", true, true, "specify row key");
        parser.addArgument("vocabularyCache", true, true, "specify row key");
        parser.addArgument("doc2len", true, true, "specify doc2len file");
        parser.parseAndCheck(args);
        loadCacheFiles(parser.getString("vocabularyCache"), parser.getString("doc2len"));
        String async_conf = parser.getString("async_conf");
        String auth_conf = parser.getString("auth_conf");
        table = CreateInverseIndexHbase.TABLE_NAME;
        String query = parser.getString("query");

        //-- SET java.security.auth.login.config --//
        System.setProperty("java.security.auth.login.config", auth_conf);


        //-- LOAD DATA FROM HBASE --//
        Config config = new Config(async_conf);
        client = new HBaseClient(config);

        String[] terms = query.split(" ");
        for (String term : terms) {
            addScoreForTerm(term);
        }
        List<FoundDocument> foundDocumentList = new ArrayList<>();
        for (Map.Entry<Integer, Double> entry : docScores.entrySet()) {
            foundDocumentList.add(new FoundDocument(entry.getKey(), entry.getValue()));
        }
        Collections.sort(foundDocumentList);
        int i = 30;
        for (FoundDocument document : foundDocumentList) {
            if (i == 0) {
                break;
            }
            if (i > 20) {
                System.out.print(document.getWikiPageTitle() + " ");
            }
            System.out.println("score:" + document.score + " https://en.wikipedia.org/?curid=" + document.id);
            i--;
        }

        client.shutdown();
    }

    private void addScoreForTerm(String term) {
        GetRequest get = new GetRequest(table, term);

        try {
            ArrayList<KeyValue> result = client.get(get).joinUninterruptibly();
            for (KeyValue res : result) {
                //load docId  (string)
                Integer docId = ByteBuffer.wrap(res.qualifier()).getInt();    //	StandardCharsets.UTF_8

                // load tf (integer)
                ByteBuffer bb = ByteBuffer.wrap(res.value());
                int tf = bb.getInt();
                double scoreForTerm = calculateBM25Score(docId, term, tf);
                Double currentScore = docScores.get(docId);
                if (currentScore == null) {
                    currentScore = 0.0;
                }
                docScores.put(docId, currentScore + scoreForTerm);
            }
        } catch (Exception e) {
            System.out.println("Get failed:");
            e.printStackTrace();
        }
    }

    private double calculateBM25Score(int documentId, String term, int freq) {
        Double up = freq * (k + 1);
        Double down = freq + k * (1 - b + b * doc2len.get(documentId) / avgdl);
        Double IDF = Math.log((N - termToDocCount.get(term) + 0.5) / (termToDocCount.get(term) + .5));
        return IDF * up / down;
    }

    private void loadCacheFiles(String termToDocCountPath, String doc2lenPath) throws IOException {
        BufferedReader cacheReader = new BufferedReader(new InputStreamReader(new FileInputStream(termToDocCountPath)));//vocabulary is first
        String strLine;
        while ((strLine = cacheReader.readLine()) != null) {
            String[] split = strLine.split("\t");
            termToDocCount.put(split[0], Integer.valueOf(split[1]));
        }

        cacheReader = new BufferedReader(new InputStreamReader(new FileInputStream(doc2lenPath)));//doc2len file
        BigDecimal docLenSum = BigDecimal.ZERO;
        N = 0L;
        while ((strLine = cacheReader.readLine()) != null) {
            String[] split = strLine.split("\t");
            doc2len.put(Integer.valueOf(split[0]), Integer.valueOf(split[1]));
            docLenSum = docLenSum.add(BigDecimal.valueOf(Integer.valueOf(split[1])));
            N++;
        }
        avgdl = docLenSum.divide(BigDecimal.valueOf(N), RoundingMode.HALF_UP).doubleValue();
        System.out.println("#doc :" + N + " avgdl:" + avgdl);
        cacheReader.close();
    }
}
