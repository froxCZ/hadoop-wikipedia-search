package cz.cvut.bigdata.step3;

import cz.cvut.bigdata.cli.ArgumentParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.RegionLocator;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 * WordCount Example, version 1.0
 * <p>
 * This is a very simple extension of basic WordCount Example implemented using
 * a new MapReduce API.
 */
public class CreateInverseIndexHbase extends Configured implements Tool {
    public static final String TABLE_NAME = "udrzalv:wiki_index";

    /**
     * The main entry of the application.
     */
    public static void main(String[] arguments) throws Exception {
        System.exit(ToolRunner.run(new CreateInverseIndexHbase(), arguments));
    }

    /**
     * Receives (byteOffsetOfLine, textOfLine), note we do not care about the type of the key
     * because we do not use it anyway, and emits (word, 1) for each occurrence of the word
     * in the line of text (i.e. the received value).
     */
    public static class MyMapper extends Mapper<Object, Text, ImmutableBytesWritable, KeyValue> {
        private final IntWritable ONE = new IntWritable(1);
        private Text word = new Text();
        private HashSet<String> validTerms = new HashSet<>();
        private MultipleOutputs mo;

        protected void setup(Context context) throws IOException, InterruptedException {
            super.setup(context);
            Configuration conf = context.getConfiguration();
            FileSystem fs = FileSystem.getLocal(conf);

            Path[] dataFile = DistributedCache.getLocalCacheFiles(conf);
            // [0] because we added just one file.
            BufferedReader cacheReader = new BufferedReader(new InputStreamReader(fs.open(dataFile[0])));
            String strLine;
            while ((strLine = cacheReader.readLine()) != null) {
                validTerms.add(strLine.split("\t")[0]);
            }
            cacheReader.close();
        }

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

            String valueStr = value.toString();
            String[] words = valueStr.toLowerCase().split(" ");
            int firstTab = valueStr.indexOf("\t");
            int docId = Integer.valueOf(valueStr.substring(0, firstTab));
            Iterator<String> wordIterator = Arrays.asList(words).iterator();
            HashMap<String, Integer> termOccurenceMap = new HashMap<>();
            while (wordIterator.hasNext()) {
                String term = wordIterator.next();
                if (validTerms.contains(term)) {
                    Integer count = termOccurenceMap.get(term);
                    if (count == null) {
                        count = 0;
                    }
                    count++;
                    termOccurenceMap.put(term, count);
                }
            }
            byte[] docBytes = Bytes.toBytes(docId);
            byte[] colFamily = Bytes.toBytes("doc");
            for (Map.Entry<String, Integer> entry : termOccurenceMap.entrySet()) {
                byte[] wordInBytes = Bytes.toBytes(entry.getKey());
                byte[] tfBytes = Bytes.toBytes(entry.getValue());
                ImmutableBytesWritable HKey = new ImmutableBytesWritable(wordInBytes);
                KeyValue kv = new KeyValue(wordInBytes, colFamily, docBytes, tfBytes);
                context.write(HKey, kv);
            }
        }
    }

        /**
     * This is where the MapReduce job is configured and being launched.
     */
    @Override
    public int run(String[] arguments) throws Exception {
        ArgumentParser parser = new ArgumentParser("CreateInverseIndexHbase");

        parser.addArgument("input", true, true, "specify input directory");
        parser.addArgument("output", true, true, "specify output directory");
        parser.addArgument("vocabularyCache", true, true, "specify output directory");

        parser.parseAndCheck(arguments);

        Path inputPath = new Path(parser.getString("input"));
        Path outputDir = new Path(parser.getString("output"));
        Path cachePath = new Path(parser.getString("vocabularyCache"));


        Configuration hconf = HBaseConfiguration.create();
        hconf.set("hadoop.tmp.dir", "/tmp");
        hconf.set("hadoop.security.authentication", "Kerberos");
        UserGroupInformation.setConfiguration(hconf);
        UserGroupInformation.getUGIFromTicketCache(System.getenv("KRB5CCNAME"), null);

        // Create job.
        DistributedCache.addCacheFile(cachePath.toUri(), hconf);
        Job job = Job.getInstance(hconf, "CreateInverseIndexHbase");

        job.setJarByClass(MyMapper.class);

        // Setup MapReduce.
        job.setMapperClass(MyMapper.class);

        // Make use of a combiner - in this simple case
        // it is the same as the reducer.
        //job.setCombinerClass(MyReducer.class);


        // By default, the number of reducers is configured
        // to be 1, similarly you can set up the number of
        // reducers with the following line.
        //
        // job.setNumReduceTasks(1);

        // Specify (key, value).
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(KeyValue.class);

        // Input.
        FileInputFormat.addInputPath(job, inputPath);
        job.setInputFormatClass(TextInputFormat.class);

        // Output.
        FileOutputFormat.setOutputPath(job, outputDir);
        //not for hbase??? job.setOutputFormatClass(TextOutputFormat.class);


        Connection connection = ConnectionFactory.createConnection(hconf);
        TableName tableName = TableName.valueOf(TABLE_NAME);
        Table hTable = connection.getTable(tableName);
        RegionLocator regionLocator = connection.getRegionLocator(tableName);
        HFileOutputFormat2.configureIncrementalLoad(job, hTable, regionLocator);
        
        FileSystem hdfs = FileSystem.get(hconf);

        // Delete output directory (if exists).
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);

        // Execute the job.
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
