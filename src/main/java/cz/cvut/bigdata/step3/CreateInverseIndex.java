package cz.cvut.bigdata.step3;

import cz.cvut.bigdata.VectorWritable;
import cz.cvut.bigdata.cli.ArgumentParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * WordCount Example, version 1.0
 * <p>
 * This is a very simple extension of basic WordCount Example implemented using
 * a new MapReduce API.
 */
public class CreateInverseIndex extends Configured implements Tool {
    public static final String KEY_VALUE_SEPARATOR = ":";
    public static final String DOC_2_LEN_OUTPUT_NAME = "doc2len";
    private static final String INVERSE_INDEX_OUTPUT_NAME = "inverseIndex";

    /**
     * The main entry of the application.
     */
    public static void main(String[] arguments) throws Exception {
        System.exit(ToolRunner.run(new CreateInverseIndex(), arguments));
    }

    /**
     * Receives (byteOffsetOfLine, textOfLine), note we do not care about the type of the key
     * because we do not use it anyway, and emits (word, 1) for each occurrence of the word
     * in the line of text (i.e. the received value).
     */
    public static class MyMapper extends Mapper<Object, Text, Text, Text> {
        private final IntWritable ONE = new IntWritable(1);
        private Text word = new Text();
        private HashSet<String> validTerms = new HashSet<>();
        private MultipleOutputs mo;

        protected void setup(Mapper.Context context) throws IOException, InterruptedException {
            super.setup(context);
            Configuration conf = context.getConfiguration();
            FileSystem fs = FileSystem.getLocal(conf);

            Path[] dataFile = DistributedCache.getLocalCacheFiles(conf);
            // [0] because we added just one file.
            BufferedReader cacheReader = new BufferedReader(new InputStreamReader(fs.open(dataFile[0])));
            String strLine;
            while ((strLine = cacheReader.readLine()) != null) {
                validTerms.add(strLine.split("\t")[0]);
            }
            cacheReader.close();
            mo = new MultipleOutputs(context);


        }

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

            String valueStr = value.toString();
            String[] words = valueStr.toLowerCase().split(" ");
            int firstTab = valueStr.indexOf("\t");
            int docId = Integer.valueOf(valueStr.substring(0, firstTab));
            Iterator<String> wordIterator = Arrays.asList(words).iterator();
            HashMap<String, Integer> termOccurenceMap = new HashMap<>();
            int documentWordCount = 0;
            while (wordIterator.hasNext()) {
                String term = wordIterator.next();
                if (validTerms.contains(term)) {
                    documentWordCount++;
                    Integer count = termOccurenceMap.get(term);
                    if (count == null) {
                        count = 0;
                    }
                    count++;
                    termOccurenceMap.put(term, count);
                }
            }
            System.out.println(docId+"\t"+documentWordCount);
            mo.write(DOC_2_LEN_OUTPUT_NAME, new LongWritable(docId), new LongWritable(documentWordCount), DOC_2_LEN_OUTPUT_NAME);
            for (Map.Entry<String, Integer> entry : termOccurenceMap.entrySet()) {
                context.write(new Text(entry.getKey()), new Text(docId + KEY_VALUE_SEPARATOR + entry.getValue()));
            }
        }
    }

    /**
     */
    public static class MyReducer extends Reducer<Text, Text, Text, Text> {
        private MultipleOutputs mo;

        protected void setup(Reducer.Context context) throws IOException, InterruptedException {
            super.setup(context);
            mo = new MultipleOutputs(context);

        }
        public void reduce(Text text, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            StringBuilder sb = new StringBuilder();
            for (Text value : values) {
                String[] split = value.toString().split(KEY_VALUE_SEPARATOR);
                sb.append(split[0] + ":" + split[1] + ",");
            }
            String list = sb.toString();

            mo.write(INVERSE_INDEX_OUTPUT_NAME, text, list.substring(0, list.length() - 1), "byFirstLetter/" + text.toString().charAt(0));
        }
    }

    /**
     * This is where the MapReduce job is configured and being launched.
     */
    @Override
    public int run(String[] arguments) throws Exception {
        ArgumentParser parser = new ArgumentParser("CreateInverseIndex");

        parser.addArgument("input", true, true, "specify input directory");
        parser.addArgument("output", true, true, "specify output directory");
        parser.addArgument("vocabularyCache", true, true, "specify output directory");

        parser.parseAndCheck(arguments);

        Path inputPath = new Path(parser.getString("input"));
        Path outputDir = new Path(parser.getString("output"));
        Path cachePath = new Path(parser.getString("vocabularyCache"));

        // Create configuration.
        Configuration conf = getConf();

        // Using the following line instead of the previous 
        // would result in using the default configuration
        // settings. You would not have a change, for example,
        // to set the number of reduce tasks (to 5 in this
        // example) by specifying: -D mapred.reduce.tasks=5
        // when running the job from the console.
        //
        // Configuration conf = new Configuration(true);

        // Create job.
        DistributedCache.addCacheFile(cachePath.toUri(), conf);
        Job job = Job.getInstance(conf, "CreateInverseIndex");

        job.setJarByClass(MyMapper.class);

        // Setup MapReduce.
        job.setMapperClass(MyMapper.class);

        job.setReducerClass(MyReducer.class);
        // Make use of a combiner - in this simple case
        // it is the same as the reducer.
        //job.setCombinerClass(MyReducer.class);


        // By default, the number of reducers is configured
        // to be 1, similarly you can set up the number of
        // reducers with the following line.
        //
        // job.setNumReduceTasks(1);

        // Specify (key, value).
        job.setOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputValueClass(Text.class);

        // Input.
        FileInputFormat.addInputPath(job, inputPath);
        job.setInputFormatClass(TextInputFormat.class);

        // Output.
        FileOutputFormat.setOutputPath(job, outputDir);
        job.setOutputFormatClass(TextOutputFormat.class);

        MultipleOutputs.addNamedOutput(job, DOC_2_LEN_OUTPUT_NAME, TextOutputFormat.class, LongWritable.class, LongWritable.class);
        MultipleOutputs.addNamedOutput(job, INVERSE_INDEX_OUTPUT_NAME, TextOutputFormat.class, Text.class, Text.class);

        FileSystem hdfs = FileSystem.get(conf);

        // Delete output directory (if exists).
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);

        // Execute the job.
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
